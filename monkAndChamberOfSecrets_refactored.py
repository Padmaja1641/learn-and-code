global selectedSpiders, spiderList, positionList 
totalSpiders,selectedSpiders = input().split()    
totalSpiders = int(totalSpiders)                
selectedSpiders = int(selectedSpiders)
spiderList = [int(value) for value in input().split()] 
positionList = list(range(1, totalSpiders + 1))  
updatedSpiderList=[]
updatedPositionList=[]

def dequeue(updatedSpiderList, updatedPositionList):
    global selectedSpiders, spiderList, positionList
    length = len(spiderList)
    if length >= selectedSpiders:
        updatedSpiderList = spiderList[0:selectedSpiders]
        updatedPositionList = positionList[0:selectedSpiders]
        spiderList = spiderList[selectedSpiders:]
        positionList = positionList[selectedSpiders:]
    else:
        updatedSpiderList = spiderList
        updatedPositionList = positionList
    return updatedSpiderList,updatedPositionList,length
    
def alter(updatedSpiderList, updatedPositionList, maxIndex): 
    del updatedSpiderList[maxIndex]
    del updatedPositionList[maxIndex]
    updatedSpiderList = [num-1 if num else 0 for num in updatedSpiderList]
    return updatedSpiderList,updatedPositionList
    
def enqueue(updatedSpiderList, updatedPositionList,length):
    global selectedSpiders, spiderList, positionList
    if length >= selectedSpiders:
        spiderList.extend(updatedSpiderList)
        positionList.extend(updatedPositionList)
    else:
        spiderList = updatedSpiderList
        positionList = updatedPositionList
    return updatedSpiderList, updatedPositionList
    
for i in range(0, selectedSpiders):
    updatedSpiderList, updatedPositionList,length = dequeue(updatedSpiderList, updatedPositionList)
    maxIndex = updatedSpiderList.index(max(updatedSpiderList))
    print (updatedPositionList[maxIndex], end= " ")
    updatedSpiderList, updatedPositionList=alter(updatedSpiderList, updatedPositionList,maxIndex)
    updatedSpiderList, updatedPositionList=enqueue(updatedSpiderList, updatedPositionList,length)