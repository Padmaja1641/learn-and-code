totalSpiders,selectedSpiders = input().split()
totalSpiders = int(totalSpiders)
selectedSpiders = int(selectedSpiders)
spiderList = [int(value) for value in input().split()]
positionList = list(range(1, totalSpiders + 1))
for i in range(0, selectedSpiders):
    length = len(spiderList)
    if length >= selectedSpiders:
        updatedSpiderList = spiderList[0:selectedSpiders]
        updatedPositionList = positionList[0:selectedSpiders]
        spiderList = spiderList[selectedSpiders:]
        positionList = positionList[selectedSpiders:]
    else:
        updatedSpiderList = spiderList
        updatedPositionList = positionList
    maxIndex = updatedSpiderList.index(max(updatedSpiderList))
    print (updatedPositionList[maxIndex], end= " ")
    del updatedSpiderList[maxIndex]
    del updatedPositionList[maxIndex]
    updatedSpiderList = [num-1 if num else 0 for num in updatedSpiderList]
    if length >= selectedSpiders:
        spiderList.extend(updatedSpiderList)
        positionList.extend(updatedPositionList)
    else:
        spiderList = updatedSpiderList
        positionList = updatedPositionList