from file_operations import *

def insert():
    print("Select Type\n1.Student\n2.Employee")
    choice = int(input())
    
    if choice is 1:
        name = input("Enter Student's Name: ")
        department = input("Enter Student's Department: ")
        student_obj = Students(name, department)
        student_obj.write_into_file()
    
    elif choice is 2:
        name = input("Enter Employee's Name: ")
        project = input("Enter Employee's Project: ")
        employee_obj = Employees(name, project)
        employee_obj.write_into_file()


def display():
    print("Select Type\n1.Student\n2.Employee")
    choice = int(input())
    
    if choice is 1:
        student_obj = Students()
        student_obj.display_all_records()
    
    elif choice is 2:
        employee_obj = Employees()
        employee_obj.display_all_records()

def search():
    print("Select Type\n1.Student\n2.Employee")
    choice = int(input())
    
    if choice is 1:
        student_name = input('Enter the name to be searched in students record: ')
        student_obj = Students(student_name)
        return student_obj.search_records()
    
    elif choice is 2:
        employee_name = input('Enter the name to be searched in employees record: ')
        employee_obj = Employees(employee_name)
        return employee_obj.search_records()

def delete():
    print("Select Type\n1.Student\n2.Employee")
    choice = int(input())
    
    if choice is 1:
        student_name = input('Enter the name to be deleted from students record: ')
        student_obj = Students(student_name)
        result = student_obj.delete_records()
        if result is False:
            print("1.Delete single record\n2.Delete multiple records\n3.Delete all")
            choice = int(input())
            if choice is 1:
                student_id = int(input("Enter the id of the record to be deleted"))
                student_obj = Students(id_num=student_id)
                student_obj.delete_single_record()
            elif choice is 2:
                id_list = input("Enter the ids to be deleted separated by space").split(' ')
                for val in id_list:
                    student_obj = Students(id_num=int(val))
                    student_obj.delete_single_record()
            elif choice is 3:
                student_obj.delete_all_records()
    
    elif choice is 2:
        employee_name = input('Enter Employee name to be deleated: ')
        employee_obj = Employees(employee_name)
        result = employee_obj.delete_records()
        if result is False:
            print("1.Delete single record\n2.Delete multiple records\n3.Delete all")
            choice = int(input())
            if choice is 1:
                employee_id = int(input("Enter the id of the record to be deleted"))
                employee_obj = Employees(id_num=employee_id)
                employee_obj.delete_single_record()
            elif choice is 2:
                id_list = input("Enter the ids to be deleted separated by space:\n").split(' ')
                for val in id_list:
                    employee_obj = Employees(id_num=int(val))
                    employee_obj.delete_single_record()
            elif choice is 3:
                employee_obj.delete_all_records()

def update():
    print("Select Type\n1.Student\n2.Employee")
    choice = int(input())

    if choice is 1:
        student_name = input('Enter the student name to be updated: ')
        student_obj = Students(student_name)
        result = student_obj.update_records()
    elif choice is 2:
        employee_name = input('Enter employee name to be updated: ')
        employee_obj = Employees(employee_name)
        result = employee_obj.update_records()


if __name__=="__main__":
    while(1):
        print("1.Enter the data to be stored in DB\n2.Search for data\n3.Display the records \n4.Update the records\n5.Delete the records\n6.Exit")
        choice = int(input())
        if choice is 1:
            insert()
        if choice is 2:
            flag=search()
            if flag is False :
                print('\nNo matching records found\n')
        if choice is 3:
            display()
        if choice is 4:
            update()
        if choice is 5:
            delete()
        if choice is 6:
            break