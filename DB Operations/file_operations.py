import pickle
class File_operations():
    def get_id(self):
        list_for_id = list(self.unpickle_database())
        try:
            last_id = int(list_for_id[-1].id)
            required_id = last_id+1
        except (AttributeError, IndexError, TypeError):
            required_id = 1
        finally:
            return required_id

    def write_into_file(self):
        file_name = type(self).__name__ + '.txt'
        with open (file_name, 'ab') as input_file:
            pass
        self.id = self.get_id()
        with open (file_name, 'ab') as input_file:
            pickle.dump(self, input_file, pickle.HIGHEST_PROTOCOL)

    def unpickle_database(self):
        file_name = type(self).__name__ + '.txt'
        with open(file_name, 'rb') as output_file:
            while True:
                try:
                    yield pickle.load(output_file)
                except EOFError:
                    break

    def display_all_records(self):
        file_name = type(self).__name__ + '.txt'
        with open(file_name, 'rb') as output_file:
            while True:
                try:
                    record = pickle.load(output_file)
                    if type(self).__name__ == "Students":
                        print(record.id,'-',record.name,'-',record.department)
                    else:
                        print(record.id,'-',record.name,'-',record.project)
                except EOFError:
                    break

    def search_records(self):
        flag = False
        file_name = type(self).__name__ + '.txt'
        with open(file_name, 'rb') as output_file:
            while True:
                try:
                    record = pickle.load(output_file)
                    if record.name == self.name:
                        if type(self).__name__ == "Students":
                            print(record.id,'-',record.name,'-',record.department)
                        else:
                            print(record.id,'-',record.name,'-',record.project)
                        flag=True
                except EOFError:
                    return flag

    def truncate_file(self):
        file_name = type(self).__name__ + '.txt'
        with open(file_name, 'r+') as input_file:
            input_file.truncate()

    def save_object(self, obj):
        file_name = type(self).__name__ + '.txt'
        with open(file_name, 'ab') as input_file:
            pickle.dump(obj, input_file, pickle.HIGHEST_PROTOCOL)

    def get_no_of_matching_objects(self, record_objs):
        obj_count = 0
        for record_obj in record_objs:
            if self.name == record_obj.name:
                obj_count+=1
        return obj_count

    def delete_records(self):
        record_objs = list(self.unpickle_database())
        no_of_matching_records = self.get_no_of_matching_objects(record_objs)
        if no_of_matching_records == 1:
            for record_obj in record_objs:
                if record_obj.name == self.name:
                    del record_objs[record_objs.index(record_obj)]
                    print("Record got deleted successfully")
                    break
        elif no_of_matching_records>1:
            print("These are the records that match your input:\n")
            self.search_records()
            return False
        elif no_of_matching_records == 0:
            print("No matching records found")
            return True
        self.truncate_file()
        for record_obj in record_objs:
            self.save_object(record_obj)
            return True

    def delete_single_record(self):
        record_objs = list(self.unpickle_database())
        for record_obj in record_objs:
            if record_obj.id == self.id:
                del record_objs[record_objs.index(record_obj)]
        self.truncate_file()
        for record_obj in record_objs:
            self.save_object(record_obj)
        print("The record with selected Id got deleted successfully")

    def delete_all_records(self):
        record_objs = list(self.unpickle_database())
        for _ in range(len(record_objs)):
            for record_obj in record_objs:
                if record_obj.name == self.name:
                    del record_objs[record_objs.index(record_obj)]
        self.truncate_file()
        for record_obj in record_objs:
            self.save_object(record_obj)

    def update_single_record(self, rec_objs):
        print("\n1.Name\n2.Department")
        choice = int(input())
        if choice is 1:
            updated_name = input('Enter new name: ')
            for rec_obj in rec_objs:
                if rec_obj.name == self.name:
                    rec_obj.name = updated_name
                    break
        if choice is 2:
            updated_department = input('Enter new Department: ')
            for rec_obj in rec_objs:
                if rec_obj.name == self.name:
                    rec_obj.department = updated_department
                    break
        self.truncate_file()
        for rec_obj in rec_objs:
            self.save_object(rec_obj)

    def update_record_using_id(self, rec_objs):
        record_id = int(input("Select the id of record that is to be updated: "))
        print("Update \n1.Name\n2.Department")
        choice = int(input())
        if choice is 1:
            updated_name = input('Enter new name: ')
            for rec_obj in rec_objs:
                if rec_obj.id == self.id:
                    rec_obj.name = updated_name
                    break
        if choice is 2:
            updated_department = input('Enter new Department: ')
            for rec_obj in rec_objs:
                if rec_obj.id == self.id:
                    rec_obj.department = updated_department
                    break
        self.truncate_file()
        for rec_obj in rec_objs:
            self.save_object(rec_obj)


    def update_records(self):
        objects = list(self.unpickle_database())
        no_of_matching_records = self.get_no_of_matching_objects(objects)
        if no_of_matching_records == 1:
            self.update_single_record(objects)
        elif no_of_matching_records>1:
            print("These are the records that match your input:\n")
            self.search_records()
            self.update_record_using_id(objects)
        elif no_of_matching_records == 0:
            print("No matching records found")



class Students(File_operations):
    def __init__(self,name = None, department = None, id_num=None):
        self.id = id_num
        self.name = name
        self.department = department

class Employees(File_operations):
    def __init__(self,name = None, project = None, id_num=None):
        self.id = id_num
        self.name = name
        self.project = project