from source_code.itt_db import *
from source_code.customers import Customer
from source_code.employees import Employees

def insert():
	print("Select Category\n1.Employee\n2.Customer")
	choice = int(input())

	if choice is 1:
		name = input("Enter Employees's Name: ")
		project = input("Enter Employees's Project: ")
		employee_object = Employees(name,project)
		db_obj = ITT_database()
		db_obj.write_into_db(employee_object)

	elif choice is 2:
		name = input("Enter Customer's Name: ")
		city = input("Enter Customer's City: ")
		customer_object = Customer(name, city)
		db_obj = ITT_database()
		db_obj.write_into_db(customer_object)

def display():
	print("Select Category\n1.Employee\n2.Customer")
	choice = int(input())
	if choice is 1:
		employee_object = Employees()
		db_obj = ITT_database()
		db_obj.read_file_to_display_records(employee_object)	
	elif choice is 2:
		customer_object = Customer()
		db_obj = ITT_database()
		db_obj.read_file_to_display_records(customer_object)

def search():
	print("Select Category\n1.Employee\n2.Customer")
	choice = int(input())
	if choice is 1:
		name = input("Enter Employees's Name: ")
		employee_object = Employees(name)
		db_obj = ITT_database()
		db_obj.search_file(employee_object)
	elif choice is 2:
		name = input("Enter Customer's Name: ")
		customer_object = Customer(name)
		db_obj = ITT_database()
		db_obj.search_file(customer_object)

def delete():
	print("Select Category\n1.Employee\n2.Customer")
	choice = int(input())
	if choice is 1:
		name=input("Enter the employee name to be deleted:")
		employee_object = Employees(name)
		db_obj = ITT_database()
		matching_ids = db_obj.search_file(employee_object)
		if len(matching_ids) > 0:
			selected_ids=[]
			select_option=int(input("1.Delete single record\n2.Delete multiple records\n3.Delete all objects\n"))
			if(select_option==1):
				selected_ids.append(int(input("Enter the Id:")))
				db_obj.delete_records(employee_object,selected_ids)
				print("----------------------------------")
				print("Deleted %d record successfully"%(len(selected_ids)))

			elif(select_option==2):
				selected_ids.extend([int(x) for x in input("Enter the ids with space:" ).split()])
				db_obj.delete_records(employee_object,selected_ids)
				print("----------------------------------")
				print("Deleted %d records successfully"%(len(selected_ids)))

			elif(select_option==3):
				selected_ids.extend(matching_ids)
				db_obj.delete_records(employee_object,selected_ids)
				print("----------------------------------")
				print("Deleted %d records successfully"%(len(selected_ids)))

	elif choice is 2:
		name=input("Enter the customer name to be deleted:")
		customer_object = Customer(name)
		db_obj = ITT_database()
		matching_ids = db_obj.search_file(customer_object)
		if len(matching_ids) > 0:
			selected_ids=[]
			select_option=int(input("1.Delete single record\n2.Delete multiple records\n3.Delete all objects"))
			if(select_option==1):
				selected_ids.append(int(input("Enter the id:")))
				db_obj.delete_records(customer_object,selected_ids)
				print("----------------------------------")
				print("Deleted %d record successfully"%(len(selected_ids)))

			elif(select_option==2):
				selected_ids.extend([int(x) for x in input("Enter the ids with space:" ).split()])
				db_obj.delete_records(customer_object,selected_ids)
				print("----------------------------------")
				print("Deleted %d records successfully"%(len(selected_ids)))

			elif(select_option==3):
				selected_ids.extend(matching_ids)
				db_obj.delete_records(customer_object,selected_ids)
				print("----------------------------------")
				print("Deleted %d records successfully"%(len(selected_ids)))

def modify():
	print("Select Category\n1.Employee\n2.Customer")
	choice = int(input())
	db_obj = ITT_database()
	if choice is 1:
		print("By Which attribute you want to modify?")
		attribute=int(input("1.By Id\n2.By Employee Name\n"))
		if(attribute==1):
			id_to_modify=int(input("Enter the id you want to modify:"))
			print("Enter the attribute that needs to modify:")
			attribute_name=int(input("1.Employee Name\n2.Employee Project\n"))
			if(attribute_name==1):
				attribute_value=input("Enter the attribute value:")
				employee_object = Employees(None,None,id_to_modify)
				db_obj.modify_record(employee_object,"name",attribute_value)
			elif(attribute_name==2):
				attribute_value=input("Enter the attribute value:")
				employee_object = Employees(None,None,id_to_modify)
				db_obj.modify_record(employee_object,"project",attribute_value)
		elif(attribute==2):
			name_to_modify=input("Enter the name you want to modify:")
			employee_object = Employees(name_to_modify)
			matching_ids=db_obj.search_file(employee_object)
			if(len(matching_ids)>0):
				id_to_modify=int(input("Enter the id you want to modify:"))
				matching_set=set(matching_ids)
				if(id_to_modify in matching_set):
					print("Enter the attribute that needs to modify:")
					attribute_name=int(input("1.Employee Name\n2.Employee Project\n"))
					if(attribute_name==1):
						attribute_value=input("Enter the attribute value:")
						employee_object = Employee(None,None,id_to_modify)
						db_obj.modify_record(employee_object,"name",attribute_value)
					elif(attribute_name==2):
						attribute_value=input("Enter the attribute value:")
						employee_object = Employees(None,None,id_to_modify)
						db_obj.modify_record(employee_object,"project",attribute_value)
				else:
					print("Sorry!! Wrong selection of Id")

	if choice is 2:
		print("By Which attribute you want to modify?")
		attribute=int(input("1.By Id\n2.By Customer Name\n"))
		if(attribute==1):
			id_to_modify=int(input("Enter the id you want to modify:"))
			print("Enter the attribute that needs to modify:")
			attribute_name=int(input("1.Customer Name\n2.Customer City\n"))
			if(attribute_name==1):
				attribute_value=input("Enter the attribute value:")
				customer_object = Customer(None,None,id_to_modify)
				db_obj.modify_record(customer_object,"name",attribute_value)
			elif(attribute_name==2):
				attribute_value=input("Enter the attribute value:")
				customer_object = Customer(None,None,id_to_modify)
				db_obj.modify_record(customer_object,"city",attribute_value)
		elif(attribute==2):
			name_to_modify=input("Enter the name you want to modify:")
			customer_object = Customer(name_to_modify)
			matching_ids=db_obj.search_file(customer_object)
			if(len(matching_ids)>0):
				id_to_modify=int(input("Enter the id you want to modify:"))
				matching_set=set(matching_ids)
				if(id_to_modify in matching_set):
					print("Enter the attribute that needs to modify:")
					attribute_name=int(input("1.Customer Name\n2.Customer City\n"))
					if(attribute_name==1):
						attribute_value=input("Enter the attribute value:")
						customer_object = Customer(None,None,id_to_modify)
						db_obj.modify_record(customer_object,"name",attribute_value)
					elif(attribute_name==2):
						attribute_value=input("Enter the attribute value:")
						customer_object = Customer(None,None,id_to_modify)
						db_obj.modify_record(customer_object,"city",attribute_value)
				else:
					print("Sorry!! Wrong selection of Id")