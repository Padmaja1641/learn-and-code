
import pickle

class ITT_database():
	def getId_from_no_of_records(self, obj):
		no_of_records_in_db = 0 
		file_name = type(obj).__name__ + '.txt'
		f=open(file_name,"ab")
		f.close()
		with open(file_name,"rb") as file:
			while True:
				try:
					record = pickle.load(file)
					no_of_records_in_db = no_of_records_in_db + 1 
				except EOFError:
					return no_of_records_in_db

	def write_into_db(self, obj):
		file_name = type(obj).__name__ + '.txt'
		obj.id_num = self.getId_from_no_of_records(obj) + 1
		with open (file_name, 'ab') as input_file:
			pickle.dump(obj, input_file, pickle.HIGHEST_PROTOCOL)

	def read_file_to_display_records(self, obj):
		file_name = type(obj).__name__ + '.txt'
		try:
			with open (file_name, 'rb') as output_file:
				while True:
					try:
						record = pickle.load(output_file)   
						if type(obj).__name__ == "Customer":    
							print(record.id_num, '\t', record.name,'\t',record.city)
						else:
							print(record.id_num, '\t', record.name,'\t',record.project)
					except EOFError:
						break
		except FileNotFoundError:
			print("No file name with", file_name)

	def search_file(self, obj):
		file_name = type(obj).__name__ + '.txt'
		matching_records_ids = []
		matching_records = 0
		with open (file_name, 'rb') as output_file:
			while True:
				try:
					record = pickle.load(output_file) 
					if record.name==obj.name:
						if type(obj).__name__ == "Customer":    
							print(record.id_num, '\t', record.name,'\t',record.city)
							matching_records = matching_records + 1
							matching_records_ids.append(record.id_num)
						else:
							print(record.id_num, '\t', record.name,'\t',record.project)
							matching_records = matching_records + 1
							matching_records_ids.append(record.id_num)
				except EOFError:
					if matching_records is 0:
						print("----------------------------------")
						print("No record found with searched name")
						# print("----------------------------------")
					return matching_records_ids

	def get_objects(self,obj):
		file_name = type(obj).__name__ + '.txt'
		objects_list=[]
		with open(file_name,"rb") as file:
			while True:
				try:
					record=pickle.load(file)
					objects_list.append(record)
				except EOFError:
					return objects_list

	def remove_existing_data_in_file(self,obj):
		file_name = type(obj).__name__ + '.txt'
		with open(file_name,"rb+") as file:
			file.truncate()

	def save_list_objects_into_file(self,obj_list,obj):
		file_name = type(obj).__name__ + '.txt'
		for obj in obj_list:
			self.write_into_db(obj)

	def delete_records(self,obj,selected_ids):
		objects_list=self.get_objects(obj)
		for id_num in selected_ids:
			index=0
			for obj in objects_list:
				if(obj.id_num==id_num):
					del objects_list[index]
					break
				else:
					index=index+1
		self.remove_existing_data_in_file(obj)
		self.save_list_objects_into_file(objects_list,obj)

	def modify_record(self,object,attribute_to_be_modified,attribute_value):
		file_name = type(object).__name__ + '.txt'
		objects_list=self.get_objects(object)
		for obj in objects_list:
			if(obj.id_num==object.id_num):
				setattr(obj,attribute_to_be_modified,attribute_value)
				break
		self.remove_existing_data_in_file(object)
		self.save_list_objects_into_file(objects_list,object)
		print("successfully modified the id:%d"%(obj.id_num))
    