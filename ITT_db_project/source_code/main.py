import sys
import os
sys.path.append(os.path.dirname(os.getcwd()))
from user_interface import *

def main():
	while(1):
		print("1.Insert record\n2.Display records\n3.Search\n4.Delete\n5.Modify\n6.Exit")
		choice = int(input("Enter:"))
		if choice is 1:
			insert()
		if choice is 2:
			display()
		if choice is 3:
			search()
		if choice is 4:
			delete()
		if choice is 5:
			modify()
		if choice is 6:
			break
		print("----------------------------------")
			
if __name__ == '__main__':
	main() 
