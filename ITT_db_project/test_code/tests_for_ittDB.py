import sys
import os
sys.path.append(os.path.dirname(os.getcwd()))
print(os.path.dirname(os.getcwd()))
import unittest
from source_code.customers import Customer
from source_code.employees import Employees
from source_code.itt_db import *
import pickle
import io

class Test_insert_method(unittest.TestCase):

	def setUp(self):
		self.customer_1 = Customer(name= "Padmaja", city= "Bangalore")
		self.db_obj = ITT_database()
		self.db_obj.write_into_db(self.customer_1)	
	def tearDown(self):
		self.dir = os.listdir(os.getcwd())
		for file in self.dir:
			if file == "Customer.txt":
				os.remove(file)
	def test_insert_objects(self):
		with open("Customer.txt", "rb") as output_file:
			record = pickle.load(output_file)
			self.assertEqual(type(self.customer_1), type(record))
			self.assertEqual(self.customer_1.name, record.name)
			self.assertEqual(self.customer_1.city, record.city)
			self.assertEqual(self.customer_1.id_num, record.id_num)
			
	def test_getId(self):
		file_name = type(self.customer_1).__name__ + '.txt'
		with open(file_name, 'ab'):
			self.db_obj = ITT_database()
			id = self.db_obj.getId_from_no_of_records(self.customer_1)
			self.assertEqual(id, 1)

class Test_display_method(unittest.TestCase):

	def setUp(self):
		self.customer_1 = Customer(name= "Padmaja", city = 'Bangalore')
		self.customer_2 = Customer(name="Jhansi", city = 'Bangalore')
		self.db_obj = ITT_database()
		self.db_obj.write_into_db(self.customer_1)	
		self.db_obj.write_into_db(self.customer_2)
	def tearDown(self):
		self.dir = os.listdir(os.getcwd())
		for file in self.dir:
			if file == "Customer.txt":
				os.remove(file)
	def test_display_all_records(self):
		saved_obj = io.StringIO()
		sys.stdout = saved_obj
		self.db_obj = ITT_database()
		self.db_obj.read_file_to_display_records(self.customer_1)
		sys.stdout = sys.__stdout__
		self.assertEqual(saved_obj.getvalue(), '1 \t Padmaja \t Bangalore\n2 \t Jhansi \t Bangalore\n')

class Test_serach_records(unittest.TestCase):

	def setUp(self):
		self.customer_1 = Customer(name= "Padmaja", city= 'Bangalore')
		self.db_obj = ITT_database()
		self.db_obj.write_into_db(self.customer_1)

	def tearDown(self):
		self.dir = os.listdir(os.getcwd())
		for file in self.dir:
			if file == "Customer.txt":
				os.remove(file)

	def test_search_method(self):
		saved_obj = io.StringIO()
		sys.stdout = saved_obj
		self.db_obj = ITT_database()
		return_value = self.db_obj.search_file(self.customer_1)
		sys.stdout = sys.__stdout__
		self.assertEqual(return_value, [1])

# class Test_delete_records(unittest.TestCase):
# 	print("Test inside delete record")
# 	def setUp(self):
# 		self.customer_1 = Customer(name= "Padmaja", city = "Bangalore")
# 		self.customer_2 = Customer(name = "Jhansi", city = "Bangalore")
# 		self.db_obj = ITT_database()
# 		self.db_obj.write_into_db(self.customer_1)
# 		self.db_obj.write_into_db(self.customer_2)

# 	def tearDown(self):
# 		self.dir = os.listdir(os.getcwd())
# 		for file in self.dir:
# 			if file == "Customer.txt":
# 				os.remove(file)

# 	def Test_delete_method(self):
# 		saved_obj = io.StringIO()
# 		sys.stdout = saved_obj
# 		self.db_obj = ITT_database()
# 		return_value = self.db_obj.delete_records(self.customer_2)
# 		sys.stdout = sys.__stdout__
# 		self.assertEqual(saved_obj.getvalue(), ("successfully modified the id:%d"%(obj.id_num)))
# 		self.assertEqual(return_value, True)

